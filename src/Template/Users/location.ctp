<div class="brclr"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<h4>Your track</h4>
			</li>
		</ol>
		<p id='address'></p> 
		<div id="dvMap" style="width: 85%;height: 76%;position: absolute;overflow: hidden;"></div>
	</div>
</div>
<script async defer src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCVRyY48zhHhw0k7ZtsuDFGfPzGZheGKZ8"></script>
<script type="text/javascript">
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (p) {
			var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
			var refreshId = setInterval("saveLocation("+p.coords.latitude+","+p.coords.longitude+")", 5000	);
			var mapOptions = {
				center: LatLng,
				zoom: 20,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
			var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
			var marker = new google.maps.Marker({
				position: LatLng,
				map: map,
				icon: iconBase + 'man.png',
				title: "Your location: "+p.coords.name+" \n Latitude: " + p.coords.latitude + "\n Longitude: " + p.coords.longitude
			});
			google.maps.event.addListener(marker, "click", function (e) {
				var infoWindow = new google.maps.InfoWindow();
				infoWindow.setContent(marker.title);
				infoWindow.open(map, marker);
			});
		});
	} else {
		alert('Geo Location feature is not supported in this browser.');
	}

	function saveLocation(lat,long) {
		var location = [{
			"lat": lat,
			"long": long
		}];
        $.ajax({
            url:'saveLocation',
            method:'POST',
            dataType: "JSON",
            data: { latlong: location}
        })
        .done(function (msg) {});
	}
</script>
