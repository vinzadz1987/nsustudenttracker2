<div class="brclr"></div>
<div class="content-wrapper">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <h4>Your Location</h4>
            </li>
        </ol>
        <p id='address'></p> 
        <div id="map" style="width: 85%;height: 76%;position: absolute;overflow: hidden;"></div>
    </div>
</div>
<script async defer src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCVRyY48zhHhw0k7ZtsuDFGfPzGZheGKZ8&callback=initMap"></script>
<script type="text/javascript">


      // The following example creates complex markers to indicate beaches near
      // Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
      // to the base of the flagpole.

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: -33.9, lng: 151.2}
        });

        setMarkers(map);
      }

      // Data for the markers consisting of a name, a LatLng and a zIndex for the
      // order in which these markers should display on top of each other.
      var beaches = [
        ['Bondi Beach', -33.890542, 151.274856, 4],
        ['Coogee Beach', -33.923036, 151.259052, 5],
        ['Cronulla Beach', -34.028249, 151.157507, 3],
        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        ['Maroubra Beach', -33.950198, 151.259302, 1]
      ];

      function setMarkers(map) {
        // Adds markers to the map.

        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.

        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        var image = {
          url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
          // This marker is 20 pixels wide by 32 pixels high.
          size: new google.maps.Size(20, 32),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(0, 32)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var shape = {
          coords: [1, 1, 1, 20, 18, 20, 18, 1],
          type: 'poly'
        };
        for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i];
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            icon: image,
            shape: shape,
            title: beach[0],
            zIndex: beach[3]
          });
        }
      }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (p) {
            var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
            var refreshId = setInterval("saveLocation("+p.coords.latitude+","+p.coords.longitude+")", 5000  );
            var mapOptions = {
                center: LatLng,
                zoom: 20,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: iconBase + 'man.png',
                title: "Your location: "+p.coords.name+" \n Latitude: " + p.coords.latitude + "\n Longitude: " + p.coords.longitude
            });
            google.maps.event.addListener(marker, "click", function (e) {
                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(marker.title);
                infoWindow.open(map, marker);
            });
        });
    } else {
        alert('Geo Location feature is not supported in this browser.');
    }

	function saveLocation(lat,long) {
		var location = [{
			"lat": lat,
			"long": long
		}];
//         console.log(data);
//         var dataArray = ['location','df'];
//         // The following object masquerades as an array.
// var fakeArray = { "length": 2, 0: "Addy", 1: "Subtracty" };
 
// // Therefore, convert it to a real array
// var realArray = $.makeArray( fakeArray )

// console.log(realArray);
 
// // Now it can be used reliably with $.map()
// $.map( realArray, function( val, i ) {
//   // Do something
//   // console.log
// });
        // console.log(JSON.stringify(dataArray));
        $.ajax({
            url:'<?php echo BASE_URL ?>'+'saveLocation',
            method:'POST',
            dataType: "JSON",
            data: { latlong: location}
        })
        .done(function (msg) {});
    }
</script>
